local FpsLabel = require("core.FpsLabel")
local MapScreen = require("core.mapscreen.MapScreen")
local TextureLoader = require("core.mapscreen.TextureLoader")
local LPCSpriteRendererFactory = require("core.mapscreen.LPCSpriteRendererFactory")
local xdebug = require("core.xdebug")
local GameMapController = require("game.GameMapController")
local utils = require("core.utils")

local drawables = {}
local updatables = {}

local function addUpdatable(component)
    table.insert(updatables, component)
end

local function addDrawable(component)
    table.insert(drawables, component)
end

function love.keypressed(key)
    if key == "escape" then
        love.event.quit()
    end
end

function love.load(args)
    if args[1] == "play" then
        love.window.setMode(512, 512)

        local textureLoader = TextureLoader()
        local rendererFactories = {
            LPCSprite = LPCSpriteRendererFactory.new(textureLoader)
        }
        local screen = MapScreen.new(
                "game/maps/map001.tmx",
                textureLoader,
                rendererFactories,
                GameMapController.new()
        )

        addDrawable(screen)
        addUpdatable(screen)

        local fpsLabel = FpsLabel()
        addUpdatable(fpsLabel)
        addDrawable(fpsLabel)
    elseif args[1] then
        require(args[1])
        love.event.quit()
    else
        print("nothing do do")
        love.event.quit()
    end
end

function love.update(dt)
    xdebug:beforeUpdate(dt)
    for _, updatable in ipairs(updatables) do
        updatable:update(dt)
    end
    xdebug:afterUpdate(dt)
end

function love.draw()
    for _, drawable in ipairs(drawables) do
        drawable:draw()
    end
    xdebug:draw()
end
