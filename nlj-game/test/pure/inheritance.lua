local Animal = function()
    local self = {}

    function self:say()
        return "What does the " .. self.getType() .. " say?"
    end

    return self
end

local Fox = function()
    local self = setmetatable({}, {
        __index = Animal()
    })
    function self:getType()
        return "FOX"
    end
    return self
end

assert(Fox():say() == "What does the FOX say?")
print("ok")