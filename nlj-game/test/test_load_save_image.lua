print("Hello!")
io.flush()

local image = love.graphics.newImage("ext/lpc/torso/plate/chest_male.png")
local canvas = love.graphics.newCanvas(128, 128)

love.graphics.setCanvas(canvas)

local x = 0
local y = 0
local scale = { x = 1, y = 1 }
local origin = { x = 0, y = 0 }

love.graphics.draw(
        image,
        x,
        y,
        0,
        scale.x, scale.y,
        origin.x, origin.y,
        0, 0)
love.graphics.setCanvas()

local function saveCanvasToFile(canvas, file)
    local data = canvas:newImageData()
    local tempFile = "temp.png"
    data:encode("png", file)

    os.rename(love.filesystem.getSaveDirectory() .. "/" .. tempFile, file)
end

saveCanvasToFile(canvas, "../a/test123.png")


--print(love.filesystem.getSaveDirectory())
--love.filesystem.remove()
print("Bye!")
io.flush()