local xml2lua = require("ext.xml2lua.xml2lua")
local simplexml = require("core.contrib2ext.xml2lua.simpledom")
local utils = require("core.utils")

local map001 = "assets/maps/map001.tmx"

local handler = simplexml:new()
local xml = xml2lua.loadFile(map001)
local parser = xml2lua.parser(handler)
parser:parse(xml)
print(utils.tostring(handler.root))
io.flush()