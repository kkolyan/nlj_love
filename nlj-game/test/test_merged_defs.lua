local critters = require("game.critters")
local utils = require("core.utils")
local defutils = require("core.defutils")

local merged = defutils.mergeDefs(critters)
print(utils.tostring(merged))