local TextureLoader = require("core.mapscreen.TextureLoader")
local defutils = require("core.defutils")
local spriteSpec = require("core.lpc.sprite.spec")
local LPCSpriteRendererFactory = require("core.mapscreen.LPCSpriteRendererFactory")
local utils = require("core.utils")

local textureLoader = TextureLoader()

local CRITERS_TILE_SIZE = 64
local ORIGIN_Y_FROM_BOTTOM = 1.0 / 8

---@type table<string,CompositeSprite>
local sprites = {}

---@type ObjectDef[]
local critters = defutils.mergeDefs(require("game.critters"))

local spriteFactory = LPCSpriteRendererFactory.new(textureLoader)

for _, objectDef in ipairs(critters) do
    local sprite = spriteFactory:createRenderer(objectDef)
    sprites[objectDef.name] = sprite
end

local maxWidth = 0
local maxHeight = 0
for _, critter in ipairs(critters) do
    maxWidth = math.max(maxWidth, math.floor(critter.spriteDef.scale.x * CRITERS_TILE_SIZE))
    maxHeight = math.max(maxHeight, math.floor(critter.spriteDef.scale.y * CRITERS_TILE_SIZE))
end

local cw = #spriteSpec.orientations * maxWidth
local ch = #critters * maxHeight

utils.drawToFile("devenv/map_tiles/critters.png", cw, ch, function()
    utils.printf("writing tileset img to file...")
    for critterIndex, critter in ipairs(critters) do

        local sprite = sprites[critter.name]

        for orientationIndex, orientation in ipairs(spriteSpec.orientations) do
            sprite:setPosition({
                x = (orientationIndex - 1) * maxWidth + maxWidth / 2,
                y = (critterIndex - ORIGIN_Y_FROM_BOTTOM) * maxHeight
            })
            sprite:setOrientationByVector(orientation.initialVector)
            sprite:setActiveClip("stand")
            sprite:update(0)
            sprite:draw()
        end
    end
end)


local vars = {}
vars.imageRelativePath = "../../devenv/map_tiles/critters.png"
vars.tileOffsetX = math.floor(-maxWidth / 2)
vars.tileOffsetY = math.floor(maxHeight * ORIGIN_Y_FROM_BOTTOM)
vars.tileWidth = maxWidth
vars.tileHeight = maxHeight
vars.textureWidth = cw
vars.textureHeight = ch

local tileSetTemplate = [[<?xml version="1.0" encoding="UTF-8"?>
<tileset name="critters" tilewidth="$tileWidth" tileheight="$tileHeight" tilecount="12" columns="4">
 <tileoffset x="$tileOffsetX" y="$tileOffsetY"/>
 <image source="$imageRelativePath" width="$textureWidth" height="$textureHeight"/>
$tiles</tileset>]]
local tileTemplate = [[
 <tile id="$id" type="character">
  <properties>
   <property name="name" value="$critterName"/>
   <property name="orientation" value="$orientationName"/>
  </properties>
 </tile>
]]
local tiles = ""
local tileId = 0
for _, critter in ipairs(critters) do
    for _, orientation in ipairs(spriteSpec.orientations) do
        vars.id = tileId
        vars.critterName = critter.name
        vars.orientationName = orientation.name
        tiles = tiles..utils.mergeTemplate(tileTemplate, vars)
        tileId = tileId + 1
    end
end
vars.tiles = tiles

local s = utils.mergeTemplate(tileSetTemplate, vars)

print("writing tileset def to file...")
utils.writeToFileText("game/map_tiles/critters.tsx", s)

print("Done")