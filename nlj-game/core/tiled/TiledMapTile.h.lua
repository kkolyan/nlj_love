---@class TiledMapTile
---@field atlasPath string
---@field atlasSize Vec2
---@field position Vec2
---@field cell Vec2
---@field region Rectangle
---@field layerName string
local TiledMapTile