
---@class TiledMapLayer
---@field objects TiledMapObject[]
---@field tiles TiledMapTile[]
---@field name
---@field type
local TiledMapLayer = {}