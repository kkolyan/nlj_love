local xml = require("core.xml")
local utils = require("core.utils")

local TiledMap = {}

---@class TiledMap_TileSet
---@field tileWidth number
---@field tileHeight number
---@field firstgid number
---@field atlasPath string
---@field atlasSize Vec2
local TileSet

---@class TileMap_TileTemplate
---@field gid number
---@field firstgid number
---@field region Rectangle
local TileTemplate

function TiledMap.loadMap(file)

    ---@class TiledMap
    local self = {}

    -- private fields

    ---@type TiledMapLayer[]
    local layers = {}

    -- public methods

    function self:getLayer(name)
        for _, layer in ipairs(layers) do
            if layer.name == name then
                return layer
            end
        end
        utils.errorf("layer not found: %s. available: %s", name, layers)
    end

    ---@return TiledMapLayer[]
    function self:getLayers()
        return { unpack(layers) }
    end

    -- constructor

    ---@type TileMap_TileTemplate[]
    local tilesTemplatesByGid = {}

    ---@type table<number,TiledMap_TileSet>
    local tileSetsByFirstgid = {}
    local propsByGid = {}

    ---@param tilesetTag ImmutableXmlNode
    local function addTileSet(tilesetTag)
        local reservedTilePropNames = { "id", "type" }

        local firstgid = tonumber(tilesetTag:attr("firstgid"))
        local tileSetSource = tilesetTag:attr("source")
        if tileSetSource then
            local tilesetDocument = xml.parseFile(file .. "../../" .. tileSetSource)
            tilesetTag = tilesetDocument:child("tileset")
            local tileTags = tilesetTag:children("tile")
            for _, tileTag in ipairs(tileTags) do
                local propertyTags = tileTag:child("properties"):children("property")
                local propsOfTile = {}
                for _, propertyTag in ipairs(propertyTags) do
                    local attrName = propertyTag:attr("name")
                    for _, reservedPropName in ipairs(reservedTilePropNames) do
                        if attrName == reservedPropName then
                            utils.errorf("property name %s is reserved. reserved property names: %s", attrName, reservedTilePropNames)
                        end
                    end
                    propsOfTile[attrName] = propertyTag:attr("value")
                end
                propsOfTile.type = tileTag:attr("type")
                local gid = firstgid + tileTag:attr("id")
                propsByGid[gid] = propsOfTile
            end
        end
        local tileWidth = tonumber(tilesetTag:attr("tilewidth"))
        local tileHeight = tonumber(tilesetTag:attr("tileheight"))

        local image = tilesetTag:child("image")

        local width = tonumber(image:attr("width"))
        local height = tonumber(image:attr("height"))

        local cols = width / tileWidth
        local rows = height / tileHeight

        ---@type TiledMap_TileSet
        local ts = {}
        ts.name = tilesetTag:attr("name")
        ts.tileWidth = tileWidth
        ts.tileHeight = tileHeight
        ts.firstgid = firstgid
        ts.atlasPath = utils.normalizePath("game/maps/" .. image:attr("source"))
        ts.atlasSize = { x = width, y = height }
        tileSetsByFirstgid[firstgid] = ts

        for row_0b = 0, rows - 1 do
            for col_0b = 0, cols - 1 do
                local gid = firstgid + col_0b + row_0b * cols

                ---@type TileMap_TileTemplate
                local tt = {}
                tt.gid = gid
                tt.firstgid = firstgid
                tt.region = {}
                tt.region.x = col_0b * tileWidth
                tt.region.y = row_0b * tileHeight
                tt.region.width = tileWidth
                tt.region.height = tileHeight
                tilesTemplatesByGid[gid] = tt
            end
        end
    end

    local function addTileLayer(layerTag)
        ---@type TiledMapLayer
        local layer = {}
        layer.name = layerTag:attr("name")
        layer.tiles = {}
        layer.type = 'tile'

        local data = layerTag:child("data")
        for row, line in ipairs(utils.split(data:text(), "[^\r\n]+")) do
            for col, cell in ipairs(utils.split(line, "[^,]+")) do
                if cell ~= "0" then
                    local tileTemplate = tilesTemplatesByGid[tonumber(cell)]
                    local tileSet = tileSetsByFirstgid[tileTemplate.firstgid]

                    ---@type TiledMapTile
                    local tile = {}
                    tile.layerName = layer.name
                    tile.atlasPath = tileSet.atlasPath
                    tile.atlasSize = tileSet.atlasSize
                    tile.region = tileTemplate.region
                    tile.position = {}
                    tile.position.x = (col - 1) * tileSet.tileWidth
                    tile.position.y = (row - 1) * tileSet.tileHeight
                    tile.cell = {x = col, y = row}
                    table.insert(layer.tiles, tile)
                end
            end
        end
        table.insert(layers, layer)
    end

    local function addObjectLayer(objectgroupTag)
        ---@type TiledMapLayer
        local layer = {}
        layer.name = objectgroupTag:attr("name")
        layer.objects = {}
        layer.type = 'object'

        for _, objectTag in ipairs(objectgroupTag:children()) do
            local gid = tonumber(objectTag:attr("gid"))
            local props = propsByGid[gid]

            if props.x or props.y then
                error("attempt ot use restricted proeprty names (x, y): " .. utils.tostring(props))
            end
            ---@type TiledMapObject
            local object = {}
            object.x = tonumber(objectTag:attr("x"))
            object.y = tonumber(objectTag:attr("y"))
            for k, v in pairs(props) do
                object[k] = v
            end
            utils.assert(object.name, props)
            table.insert(layer.objects, object)
        end
        table.insert(layers, layer)
    end

    local document = xml.parseFile(file)

    local mapTag = document:child("map")

    for _, element in ipairs(mapTag:children()) do
        if element:getTagName() == "tileset" then
            addTileSet(element)
        elseif element:getTagName() == "layer" then
            addTileLayer(element)
        elseif element:getTagName() == "objectgroup" then
            addObjectLayer(element)
        else
            error("unexpected element: " .. utils.tostring(element))
        end
    end

    return self
end

return TiledMap