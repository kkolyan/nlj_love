---@class TiledMapObject : table<string,any>
---@field x number
---@field y number
---@field name string
---@field type string
local TiledMapObject