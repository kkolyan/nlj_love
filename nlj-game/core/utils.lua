local xdebug = require("core.xdebug")
local utils = {}

function utils.tostring(value, indent)
    if not indent then
        indent = "  "
    end
    if not value then
        if type(value) == "nil" then
            return "nil"
        elseif type(value) == "boolean" then
            return "false"
        else
            error(value)
        end
    end
    if type(value) == "string" then
        return "[[" .. value .. "]]"
    end
    if type(value) == "table" then
        if value.__tostring then
            return utils.tostring(value:__tostring())
        end
        local s = "{"
        local lengthBefore = #s
        local n = 0
        for k, v in pairs(value) do
            if n > 0 then
                s = s .. ", "
            end
            s = s .. "\n" .. indent
            local ks
            if type(k) == "number" then
                ks = "[" .. tostring(k) .. "]"
            else
                ks = tostring(k)
            end
            s = s .. ks .. " = " .. utils.tostring(v, indent .. "  ")
            n = n + 1
        end
        if #s > lengthBefore then
            s = s .. "\n" .. string.sub(indent, math.min(3, #indent + 1), #indent)
        end
        return s .. "}"
    end
    return tostring(value)
end

function utils.split(text, expr)
    local lines = {}
    for line in text:gmatch(expr) do
        table.insert(lines, line)
    end
    return lines
end

function utils.vector(pair)
    return { x = pair[1], y = pair[2] }
end

function utils.vectorAngleDegrees(v)
    return math.atan2(v.y, v.x) * 180 / math.pi
end

function utils.vectorOfAngle(angleDeg, amp)
    local angleRad = angleDeg / 180 * math.pi
    return { x = amp * math.cos(angleRad), y = amp * math.sin(angleRad) }
end

function utils.vectorLength(v)
    return math.sqrt((v.x * v.x + v.y * v.y))
end

function utils.vectorDistance(a, b)
    return utils.vectorLength(utils.vectorMinus(a, b))
end

function utils.vectorMinus(a, b)
    return { x = a.x - b.x, y = a.y - b.y }
end

function utils.vectorMultiply(v, a)
    return { x = v.x * a, y = v.y * a }
end

function utils.round(n)
    return math.floor((math.floor(n * 2) + 1) / 2)
end

function utils.vectorNormalize(v)
    local length = utils.vectorLength(v)
    return { x = v.x / length, y = v.y / length }
end

function utils.normalizePath(path)
    local results = {}
    for _, part in ipairs(utils.split(path, "[^/]*")) do
        if part == "" or part == "." then
        elseif part == ".." then
            table.remove(results, #results)
        else
            table.insert(results, part)
        end
    end
    return table.concat(results, "/")
end

function utils.parentPath(path)
    local elements = utils.split(path, "[^/]*")
    local parentElements = utils.sublist(elements, 1, #elements - 1)
    return table.concat(parentElements, "/")
end

---@generic T
---@param list T[]
---@return table<string,T>
function utils.indexListBy(list, property)
    local results = {}
    if type(property) == "function" then
        for _, v in ipairs(list) do
            results[property(v)] = v
        end
    elseif type(property) == "string" then
        for _, v in ipairs(list) do
            results[v[property]] = v
        end
    else
        error("unsupported property type: " .. property)
    end
    return results
end

---@generic T
---@param list T[]
---@return T[]
function utils.sublist(source, from, to)
    local result = {}
    for i = from, math.min(to, #source) do
        table.insert(result, source[i])
    end
    return result
end

function utils.error(msg, level)
    error(utils.tostring(msg), level)
end

function utils.errorf(fmt, ...)
    error(utils.format(fmt, ...))
end

function utils.assertEq(expected, actual)
    assert(expected == actual, utils.tostring({
        expected = expected,
        actual = actual
    }))
end

function utils.assert(v, msg)
    assert(v, utils.tostring(msg))
end

function utils.format(fmt, ...)
    local args = { n = select("#", ...), ... }
    local a = {}
    for i = 1, args.n do
        table.insert(a, utils.tostring(args[i]))
    end
    return string.format(fmt, unpack(a))
end

function utils.deepClone(def)
    if type(def) == "table" then
        local clonedTable = {}
        for k, v in pairs(def) do
            clonedTable[k] = utils.deepClone(v)
        end
        return clonedTable
    end
    if type(def) == "nil" or type(def) == "string" or type(def) == "number" or type(def) == "boolean" then
        return def
    end
    error("deepClone support only value types, but given " .. type(def) .. ": " .. utils.tostring(def))
end

function utils.mergeTables(...)
    local result = {}
    for _, source in ipairs({ ... }) do
        for k, v in pairs(source) do
            result[k] = v
        end
    end
    return result
end

function utils.inherit(parent)
    return setmetatable({}, { __index = parent })
end

function utils.createConstructor(class)
    return function(...)
        local newInstance = setmetatable({}, { __index = class })
        local args = { ... }
        if args[1] == class then
            error("constructor should be called with dot instead of colon. args: " .. utils.tostring(args))
        end
        newInstance:__init__(...)
        return newInstance
    end
end

function utils.createClass(parentClass)
    return setmetatable({}, { __index = parentClass })
end

function utils.printf(fmt, ...)
    print(utils.format(fmt, ...))
    io.flush()
end
function utils.print(a)
    print(utils.tostring(a))
    io.flush()
end

function utils.locals()
    local variables = {}
    local idx = 1
    while true do
        local ln, lv = debug.getlocal(2, idx)
        if ln ~= nil then
            variables[ln] = lv
        else
            break
        end
        idx = 1 + idx
    end
    return utils.tostring(variables)
end

utils.debugPhysics = false

---@param p1 Vec2
---@param p2 Vec2
function utils.debugLine(x1, y1, x2, y2)
    xdebug.debugLine(x1, y1, x2, y2)
end

function utils.debugCircle(x, y, r)
    xdebug.debugCircle(x, y, r)
end

function utils.drawToFile(filepath, canvasWidth, canvasHeight, callback)

    local canvas = love.graphics.newCanvas(
            canvasWidth,
            canvasHeight)
    love.graphics.setCanvas(canvas)
    callback()
    love.graphics.setCanvas()

    local data = canvas:newImageData()
    local tempFile = "temp.png"
    data:encode("png", tempFile)

    os.remove(filepath)
    os.rename(love.filesystem.getSaveDirectory() .. "/" .. tempFile, filepath)

    utils.printf("image written to file: %s", filepath)
end

function utils.writeToFileText(filepath, text)
    local file = io.open(filepath, "w")
    file:write(text)
    file:close()
    utils.printf("text written to file: %s", filepath)
end

function utils.mergeTemplate(template, vars)

    local s = template
    for k, v in pairs(vars) do
        s = string.gsub(s, "$" .. k, v)
    end
    return s
end

return utils