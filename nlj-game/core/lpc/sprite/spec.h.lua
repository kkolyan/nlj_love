---@class SpriteSpec
---@field itemRenderingOrder
---@field sequences SequenceSpec[]
---@field clips table<string,ClipSpec>
---@field orientations OrientationSpec[]
local SpriteSpec

---@class SequenceSpec
---@field name string
---@field length number
---@field sided boolean
local SequenceSpec

---@class ClipSpec
---@field sequence string
---@field loop boolean
---@field range number[]
---@field reversed boolean
---@field overstay number
---@field speed number
local ClipSpec

---@class OrientationSpec
---@field name string
---@field initialVector Vec2
---@field ranges number[][]
local OrientationSpec
