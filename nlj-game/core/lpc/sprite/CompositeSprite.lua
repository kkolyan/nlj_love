local utils = require("core.utils")
local spriteSpec = require("core.lpc.sprite.spec")

local CompositeSprite = {}

function CompositeSprite.new(objectDefName, layers, spriteFps, scale, clips)
    --- @class CompositeSprite : Renderer
    local self = {
        objectDefName = objectDefName, --  for debug purposes only
    }

    local x, y
    local origin = { x = 0, y = 0 }
    local activeClipName
    local stateTime = 0
    local orientationName
    local regionsToDraw
    local calculateClipPlaybackState
    local zIndex

    function self:__tostring()
        return utils.tostring({ critterDefName = objectDefName, x = x, y = y })
    end

    function self:update(dt)
        stateTime = stateTime + dt
        regionsToDraw = {}

        local playbackState = calculateClipPlaybackState()
        local activeClip = clips[activeClipName]

        for _, layer in ipairs(layers) do
            local frames = layer[activeClip.sequence]
            local frame = frames[playbackState.frameIndex]
            if not orientationName then
                error("undefined orientation")
            end
            local region = frame[orientationName]

            if not region then
                error(utils.format("failed to find region for orientation %s in frame %s", orientationName, frame))
            end

            utils.assert(region)
            table.insert(regionsToDraw, region)
        end
        utils.assert(#regionsToDraw > 0, regionsToDraw)
    end

    function self:isClipFinished()
        return calculateClipPlaybackState().finished
    end

    function calculateClipPlaybackState()

        if not activeClipName then
            error("no active clip")
        end

        local activeClip = clips[activeClipName]
        local from = activeClip.range[1]
        local to = activeClip.range[2]

        local length = to - from + 1
        assert(length > 0, "keyframes cannot be empty")
        local frameIndex_0based = math.floor(stateTime * spriteFps * (activeClip.speed or 1))

        local finished = false
        local finalIndex_0b
        if (activeClip.loop) then
            finalIndex_0b = (frameIndex_0based % length)
        else
            if frameIndex_0based >= length then
                finalIndex_0b = length - 1
                if frameIndex_0based >= length + (activeClip.overstay or 0) then
                    finished = true
                end
            else
                finalIndex_0b = frameIndex_0based
            end
        end
        if activeClip.reversed then
            finalIndex_0b = length - finalIndex_0b - 1
        end
        return { frameIndex = from + finalIndex_0b, finished = finished }
    end

    function self:draw()
        for _, region in ipairs(regionsToDraw) do
            local _, _, w, h = region.quad:getViewport()

            love.graphics.draw(
                    region.texture, region.quad,
                    x - math.floor(w * scale.x / 2),
                    y - h * scale.y * (1 - 1 / 8),
                    0,
                    scale.x, scale.y,
                    origin.x, origin.y,
                    0, 0)
        end
    end

    function self:setPosition(position)
        x = position.x
        y = position.y
    end

    function self:getX()
        return x
    end

    function self:getY()
        return y
    end

    function self:setActiveClip(clipName)
        local clip = clips[clipName]
        if not clip then
            utils.errorf("clip not found: %s. available: %s", clipName, clips)
        end
        activeClipName = clipName
        if not clip.loop then
            stateTime = 0
        end
    end

    function self:setZIndex(value)
        zIndex = value
    end

    function self:getZIndex()
        return zIndex
    end

    function self:getScaleX()
        return scale.x
    end

    function self:getScaleY()
        return scale.y
    end

    function self:setOrientationByVector(vector)
        local angle = utils.round(utils.vectorAngleDegrees(vector))
        for _, o in ipairs(spriteSpec.orientations) do
            for _, range in ipairs(o.ranges) do
                if range[1] <= angle and angle <= range[2] then
                    orientationName = o.name
                    return
                end
            end
        end
        utils.errorf("failed to determine orientation of vector: %s, with angle: %s, available orientations: %s", vector, angle, spriteSpec.orientations)
    end

    -- deny creation of new properties
    setmetatable(self, {
        __newindex = function(t, key, value)
            error("attempt to set " .. tostring(key) .. " = " .. utils.tostring(value))
        end
    })

    return self
end

return CompositeSprite