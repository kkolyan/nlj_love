local CompositeSprite = require("core.lpc.sprite.CompositeSprite")
local spriteSpec = require("core.lpc.sprite.spec")
local utils = require("core.utils")

local CompositeSpriteTemplate = {}

---@param textureLoader TextureLoader
---@param spriteDef SpriteDef
function CompositeSpriteTemplate.new(textureLoader, objectDefName, spriteDef)

    ---@class CompositeSpriteTemplate
    local self = {}
    local layers = {}

    for _, itemName in ipairs(spriteSpec.itemRenderingOrder) do
        local itemValue = spriteDef.items[itemName]
        if itemValue and itemValue ~= "-" then
            local texture = textureLoader:getTexture(itemValue)

            local maxSequenceLength = -1;
            local rowCount = 0;
            for _, sequence in ipairs(spriteSpec.sequences) do
                maxSequenceLength = math.max(maxSequenceLength, sequence.length);
                if sequence.sided then
                    rowCount = rowCount + #spriteSpec.orientations;
                else
                    rowCount = rowCount + 1;
                end
            end
            local frameWidth = texture:getWidth() / maxSequenceLength;
            local frameHeight = texture:getHeight() / rowCount;

            local handledRows = 0
            local layer = {}
            for _, sequence in ipairs(spriteSpec.sequences) do
                local keyFrames = {}

                for columnIndex = 0, sequence.length - 1 do
                    local frame = {}
                    local x = columnIndex * frameWidth;
                    if sequence.sided then
                        local orientationIndex = 0
                        for _, orientation in ipairs(spriteSpec.orientations) do
                            local y = (handledRows + orientationIndex) * frameHeight
                            local quad = love.graphics.newQuad(x, y, frameWidth, frameHeight,
                                    texture:getWidth(), texture:getHeight())

                            frame[orientation.name] = { texture = texture, quad = quad }
                            orientationIndex = orientationIndex + 1
                        end
                    else
                        local y = handledRows * frameHeight
                        local quad = love.graphics.newQuad(x, y, frameWidth, frameHeight,
                                texture:getWidth(), texture:getHeight())

                        for _, orientation in ipairs(spriteSpec.orientations) do
                            frame[orientation.name] = { texture = texture, quad = quad }
                        end
                    end
                    table.insert(keyFrames, frame)
                end
                if sequence.sided then
                    handledRows = handledRows + #spriteSpec.orientations
                else
                    handledRows = handledRows + 1
                end
                layer[sequence.name] = keyFrames
            end
            table.insert(layers, layer)
        end
    end

    function self:createSprite()
        return CompositeSprite.new(objectDefName, layers, spriteDef.fps, spriteDef.scale, spriteDef.clips)
    end

    return self
end

return CompositeSpriteTemplate