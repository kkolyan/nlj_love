---@type SpriteSpec
local spec = {
    itemRenderingOrder = {
        "behind_body",
        "skin",
        "facial",
        "feet",
        "legs",
        "hands",
        "chest",
        "arms",
        "belt",
        "accessories",
        "hair",
        "headgear",
        "weapon1",
        "weapon2"
    },
    sequences = {
        { name = "spellcast", length = 7, sided = true },
        { name = "thrust", length = 8, sided = true },
        { name = "walkcycle", length = 9, sided = true },
        { name = "slash", length = 6, sided = true },
        { name = "bow", length = 13, sided = true },
        { name = "hurt", length = 6, sided = false }
    },
    clips = {
        stand = { sequence = "walkcycle", loop = true, range = { 1, 1 } },
        walk = { sequence = "walkcycle", loop = true, range = { 2, 9 } },
        raise_aim = { sequence = "bow", loop = false, range = { 1, 9 } },
        loose_arrow = { sequence = "bow", loop = false, range = { 10, 10 } },
        grab_arrow = { sequence = "bow", loop = false, range = { 11, 13 } },
        aim = { sequence = "bow", loop = false, range = { 5, 9 } },
        hurt = { sequence = "hurt", loop = false, range = { 1, 3 } },
        unhurt = { sequence = "hurt", loop = false, range = { 1, 3 }, reversed = true },
        lay = { sequence = "hurt", loop = true, range = { 6, 6 } },
        knock = { sequence = "hurt", loop = false, range = { 1, 6 } },
        slash = { sequence = "slash", loop = false, range = { 1, 6 } },
        spellcast = { sequence = "spellcast", loop = false, range = { 1, 7 } },
        thrust_raise = { sequence = "thrust", loop = false, range = { 1, 4 } },
        thrust_put = { sequence = "thrust", loop = false, range = { 1, 4 }, reversed = true },
        thrust_loop = { sequence = "thrust", loop = false, range = { 5, 8 } },
        thrust_loop_before_hit = { sequence = "thrust", loop = false, range = { 5, 5 } },
        thrust_loop_after_hit = { sequence = "thrust", loop = false, range = { 6, 8 }, overstay = 2.1 }
    },
    orientations = {
        { name = "UP", initialVector = { x = 0, y = -1 }, ranges = { { -135, -45 } } },
        { name = "LEFT", initialVector = { x = -1, y = 0 }, ranges = { { 135, 180 }, { -180, -135 } } },
        { name = "DOWN", initialVector = { x = 0, y = 1 }, ranges = { { 45, 135 } } },
        { name = "RIGHT", initialVector = { x = 1, y = 0 }, ranges = { { -45, 44 } } }
    }
}
return spec