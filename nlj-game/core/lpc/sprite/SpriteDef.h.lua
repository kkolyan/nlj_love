---@class SpriteDef
---@field scale Vec2
---@field fps number
---@field items table<string,string>
---@field clips ClipSpec[]
local SpriteDef