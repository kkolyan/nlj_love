local utils = require("core.utils")
local xml2lua = require("ext.xml2lua.xml2lua")

local xml = {}

function xml.ImmutableXmlNode(rawNode)

    ---@class ImmutableXmlNode
    local self = {}

    local tagName = rawNode.name
    local attrs = rawNode.attrs
    local children = {}
    local text = rawNode.text

    function self:__tostring()
        return { tagName = tagName, attrs = attrs, children = children, text = text }
    end

    for _, rawChild in ipairs(rawNode.children) do
        table.insert(children, xml.ImmutableXmlNode(rawChild))
    end

    function self:getTagName()
        return tagName
    end

    function self:attr(attrName)
        return attrs[attrName]
    end

    function self:hasChild(tagName)
        local candidates = self:children(tagName)
        return #candidates > 0
    end

    ---@return ImmutableXmlNode
    function self:child(tagName)
        local candidates = self:children(tagName)
        if #candidates > 1 then
            error("expected to have no more than 1 '" .. tagName .. "', but available: " .. utils.tostring(candidates))
        end
        if #candidates < 1 then
            error("element not found: " .. tagName .. " in " .. utils.tostring(self))
        end
        return candidates[1]
    end

    ---@return ImmutableXmlNode[]
    function self:children(tagName)
        local results = {}
        for _, child in ipairs(children) do
            if not tagName or child:getTagName() == tagName then
                table.insert(results, child)
            end
        end
        return results
    end

    function self:text()
        return text
    end

    return self;
end

function xml.parseFile(file)
    file = utils.normalizePath(file)

    local handler = {}
    local stack = { { children = {} } }

    function handler:starttag(tag)
        local element = {
            name = tag.name,
            attrs = tag.attrs,
            children = {}
        }
        table.insert(stack[#stack].children, element)
        table.insert(stack, element)
    end

    function handler:endtag(tag, lineNumber)
        if tag.name ~= stack[#stack].name then
            error("XML Error - Unmatched Tag [" .. lineNumber .. ":" .. tag.name .. "]\n")
        end
        table.remove(stack, #stack)
    end

    function handler:text(t)
        stack[#stack].text = t
    end

    function handler:cdata(t)
        stack[#stack].text = t
    end

    local parser = xml2lua.parser(handler)
    local content = xml2lua.loadFile(file)
    parser:parse(content)

    return xml.ImmutableXmlNode(stack[#stack])
end

function xml.getChildrenByName(parent, childName)
    local candidates = {}
    for _, child in ipairs(parent.children) do
        if child.name == childName then
            table.insert(candidates, child)
        end
    end
    return candidates
end

function xml.getChildByName(parent, childName)
    local candidates = xml.getChildrenByName(parent, childName)
    assert(#candidates == 1, "required to have exactly 1 " .. childName .. " element, but given " .. #candidates .. ". " .. utils.tostring(parent))
    return candidates[1]
end

return xml