local utils = require("core.utils")
local defutils = {}

--- creates a recursive merged copy with priority to target values
function defutils.mergeRecursively(target, source)
    if type(source) == "nil" then
        return utils.deepClone(target)
    end
    if type(target) == "nil" then
        return utils.deepClone(source)
    end
    if type(target) == "string" or type(target) == "number" or type(target) == "boolean" then
        return target
    end
    if type(target) == "table" then
        local merged = {}
        for _, keySource in pairs({ target, source }) do
            for k, _ in pairs(keySource) do
                merged[k] = defutils.mergeRecursively(target[k], source[k])
            end
        end
        return merged
    end
    error("deepClone support only value types, but given " .. type(target) .. ": " .. utils.tostring(target))
end

---@generic T
---@param T[]
---@return T[]
function defutils.mergeDefs(defs)
    local defsByName = utils.indexListBy(defs, "name")
    local mergedDefs = {}
    for _, def in ipairs(defs) do
        local mergedDef = {}
        local level = def
        while level do
            mergedDef = defutils.mergeRecursively(mergedDef, level)
            level = defsByName[level.parent]
        end
        table.insert(mergedDefs, mergedDef)
    end
    return mergedDefs
end

return defutils