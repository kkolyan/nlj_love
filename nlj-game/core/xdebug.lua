local xdebug = {}

xdebug.enabled = true

local lines
local circles

function xdebug.debugLine(x1, y1, x2, y2)
    table.insert(lines, { x1, y1, x2, y2 })
end

function xdebug.debugCircle(x, y, r)
    table.insert(circles, { x, y, r })
end

function xdebug:beforeUpdate(dt)
    if xdebug.enabled then
        lines = {}
        circles = {}
    end
end

function xdebug:afterUpdate(dt)
end

function xdebug:draw()
    if xdebug.enabled then
        love.graphics.setLineWidth(1)
        love.graphics.setLineStyle("smooth")
        love.graphics.setColor(1, 0.5, 1, 100)

        for _, line in ipairs(lines) do
            love.graphics.line(unpack(line))
        end
        for _, circle in ipairs(circles) do
            love.graphics.circle("line", unpack(circle))
        end
        love.graphics.setColor(1, 1, 1, 100)
    end
end

return xdebug