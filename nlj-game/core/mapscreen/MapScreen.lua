local utils = require("core.utils")
local TiledMap = require("core.tiled.TiledMap")
local TileFieldLayer = require("core.mapscreen.TileFieldLayer")
local ObjectGroupLayer = require("core.mapscreen.ObjectGroupLayer")

local MapScreen = {}

---@param tmxFile string
---@param textureLoader TextureLoader
---@param objectDefs ObjectDef[]
---@param rendererFactories RendererFactory[]
---@param mapController MapController
function MapScreen.new(tmxFile, textureLoader, rendererFactories, mapController)

    ---@class MapScreen
    local self = {}

    local physicsWorld

    ---@type MapScreenLayer[]
    local layers = {}

    ---@type table<string,MapScreenLayer>
    local layersByName = {}

    --- @class ObjectEnv
    local env = {}
    function env:getPhysicsWorld()
        if not physicsWorld then
            physicsWorld = love.physics.newWorld(0, 0, false)
        end
        return physicsWorld
    end
    function env:changeLayer(object, newLayerName)
        local oldLayer = layersByName[object.mapLayerName]
        assert(oldLayer, object.mapLayerName)
        oldLayer:removeObject(object)
        local newLayer = layersByName[newLayerName]
        assert(newLayer)
        newLayer:addObject(object)
    end

    ---@param layerName string
    function env:getObjects(layerName)
        ---@type ObjectGroupLayer
        local layer = layersByName[layerName]
        if not layer.getObjects then
            utils.errorf("non object layer: %s. %s", layerName, layer)
        end
        return layer:getObjects()
    end


    ---@param layerName string
    ---@return TiledMapTile[]
    function env:getTiles(layerName)
        ---@type TileFieldLayer
        local layer = layersByName[layerName]
        if not layer.getTiles then
            utils.errorf("non tile layer: %s. %s", layerName, layer)
        end
        return layer:getTiles()
    end

    local map = TiledMap.loadMap(tmxFile)

    --utils.error(map:getLayer("critters"))

    function self:draw()
        for _, layer in ipairs(layers) do
            layer:draw()
        end
    end

    function self:update(dt)
        mapController:beforeUpdate(dt)
        for _, layer in ipairs(layers) do
            layer:update(dt)
        end
        if physicsWorld then
            physicsWorld:update(dt)
        end
        mapController:afterUpdate(dt)
    end

    ---@type table<string,MapScreenLayerFactory>
    local layerFactoryByType = {
        tile = TileFieldLayer.Factory(textureLoader),
        object = ObjectGroupLayer.Factory(rendererFactories, mapController)
    }

    for _, layerDef in ipairs(map:getLayers()) do
        local type = layerDef.type
        local layerFactory = layerFactoryByType[type]
        local layer = layerFactory:createLayer(env, layerDef)
        table.insert(layers, layer)
        local layerName = layerDef.name
        layersByName[layerName] = layer
    end

    mapController:init(env)

    return self
end

return MapScreen