local utils = require("core.utils")

local TileFieldLayer = {}

---@param originalTiles TiledMapTile[]
function TileFieldLayer.__init__(tiles, originalTiles)

    ---@class TileFieldLayer : MapScreenLayer
    local self = {}

    function self:draw()
        for _, tile in ipairs(tiles) do
            love.graphics.draw(tile.atlasTexture, tile.quad, tile.x, tile.y, 0, 1, 1, 0, 0, 0, 0)
        end
    end

    function self:getTiles()
        return originalTiles
    end

    function self:update(dt)
    end

    return self
end
function TileFieldLayer.Factory(textureLoader)
    ---@class TileFieldLayerFactory :  MapScreenLayerFactory
    local self = {}
    function self:createLayer(env, layerDef)
        local tiles = {}
        for _, tile in ipairs(layerDef.tiles) do

            local texture = textureLoader:getTexture(tile.atlasPath)

            local atlasSize = tile.atlasSize
            utils.assertEq(texture:getWidth(), atlasSize.x)
            utils.assertEq(texture:getHeight(), atlasSize.y)

            table.insert(tiles, {
                x = tile.position.x,
                y = tile.position.y,
                atlasTexture = texture,
                quad = love.graphics.newQuad(tile.region.x, tile.region.y, tile.region.width, tile.region.height, atlasSize.x, atlasSize.y)
            })
        end
        return TileFieldLayer.__init__(tiles, layerDef.tiles)
    end
    return self
end

return TileFieldLayer