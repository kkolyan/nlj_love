
---@class Renderer
local Renderer = {}

function Renderer:update(dt) end

function Renderer:draw() end

function Renderer:getZIndex() end