---@class MapController
local MapController = {}

---@param env ObjectEnv
function MapController:init(env) end

function MapController:beforeUpdate(dt) end

function MapController:afterUpdate(dt) end

---@param env ObjectEnv
---@param renderer Renderer
---@return MapObject
function MapController:createObject(env, renderer, props) end

---@return ObjectDef[]
function MapController:getObjectDefs() end
