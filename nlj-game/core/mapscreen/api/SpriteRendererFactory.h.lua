---@class RendererFactory
local RendererFactory = {}

---@param objectDef ObjectDef
---@return Renderer
function RendererFactory:createRenderer(objectDef) end