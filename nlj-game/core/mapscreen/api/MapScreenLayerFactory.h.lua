---@class MapScreenLayerFactory
local MapScreenLayerFactory = {}

---@param env ObjectEnv
---@return MapScreenLayer
---@param layerDef TiledMapLayer
function MapScreenLayerFactory:createLayer(env, layerDef) end