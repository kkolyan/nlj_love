local utils = require("core.utils")

return function()

    ---@class TextureLoader
    local self = {}
    local textureCache = {}
    function self:getTexture(texturePath)
        texturePath = utils.normalizePath(texturePath)
        local texture = textureCache[texturePath]
        if not texture then
            texture = love.graphics.newImage(texturePath)
            textureCache[texturePath] = texture
        end
        return texture
    end
    function self:getUsedTexturePaths()
        local paths = {}
        for texturePath, _ in ipairs(textureCache) do
            table.insert(paths, texturePath)
        end
        return paths
    end
    return self
end