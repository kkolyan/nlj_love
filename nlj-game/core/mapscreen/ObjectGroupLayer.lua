local utils = require("core.utils")
local defutils = require("core.defutils")

local ObjectGroupLayer = {}

local function compareZIndex(a, b)
    return a:rendererGetZIndex() < b:rendererGetZIndex()
end

---@param objects MapObject[]
function ObjectGroupLayer.__init__(objects)

    ---@class ObjectGroupLayer : MapScreenLayer
    local self = {}

    function self:draw()
        table.sort(objects, compareZIndex)
        for _, object in ipairs(objects) do
            object:rendererDraw()
        end
    end

    function self:removeObject(object)
        for i, v in ipairs(objects) do
            if v == object then
                table.remove(objects, i)
                return
            end
        end
    end

    function self:addObject(object)
        table.insert(objects, object)
    end

    function self:update(dt)
        for _, object in ipairs(objects) do
            object:beforeUpdate(dt)
        end
        for _, object in ipairs(objects) do
            object:rendererUpdate(dt)
        end
        for _, object in ipairs(objects) do
            object:afterUpdate(dt)
        end
    end

    function self:getObjects()
        return { unpack(objects) }
    end

    return self
end

---@param rendererFactories table<string,RendererFactory>
---@param mapController MapController
function ObjectGroupLayer.Factory(rendererFactories, mapController)


    ---@type ObjectDef[]
    local objectDefs = defutils.mergeDefs(mapController:getObjectDefs())

    ---@type table<string,ObjectDef>
    local objectDefsByName = utils.indexListBy(objectDefs, "name")

    ---@class ObjectGroupLayerFactory :  MapScreenLayerFactory
    local self = {}

    function self:createLayer(env, layerDef)
        ---@type MapObject[]
        local objects = {}
        local mapObjects = layerDef.objects

        for _, mapObject in ipairs(mapObjects) do
            local objectDef = objectDefsByName[mapObject.name]
            local rendererFactory = rendererFactories[objectDef.rendererName]
            local renderer = rendererFactory:createRenderer(objectDef)

            local object = mapController:createObject(env, renderer, utils.mergeTables(objectDef.props, mapObject))
            if not object then
                utils.errorf("failed to create object with props: %s", objectDef.props)
            end

            function object:rendererDraw()
                if renderer then
                    renderer:draw()
                end
            end

            function object:rendererUpdate(dt)
                if renderer then
                    renderer:update(dt)
                end
            end

            function object:rendererGetZIndex()
                if renderer then
                    return renderer:getZIndex()
                end
                return 0
            end

            assert(layerDef.name)
            object.mapLayerName = layerDef.name

            table.insert(objects, object)
        end
        return ObjectGroupLayer.__init__(objects)
    end

    return self
end

return ObjectGroupLayer