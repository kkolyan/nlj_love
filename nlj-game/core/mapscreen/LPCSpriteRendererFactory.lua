local utils = require("core.utils")
local CompositeSpriteTemplate = require("core.lpc.sprite.CompositeSpriteTemplate")
local spriteSpec = require("core.lpc.sprite.spec")
local defutils = require("core.defutils")

local LPCSpriteRendererFactory = {}

---@param textureLoader TextureLoader
function LPCSpriteRendererFactory.new(textureLoader)

    ---@type CompositeSpriteTemplate[]
    local spritesByObjectDefName = {}

    ---@class LPCSpriteRendererFactory : RendererFactory
    local self = {}
    ---@return CompositeSprite
    function self:createRenderer(objectDef)
        local spriteTemplate = spritesByObjectDefName[objectDef.name]
        if not spriteTemplate then
            local mergedSpriteDef = defutils.mergeRecursively(objectDef.spriteDef, { clips = spriteSpec.clips })
            spriteTemplate = CompositeSpriteTemplate.new(textureLoader, objectDef.name, mergedSpriteDef)
            spritesByObjectDefName[objectDef.name] = spriteTemplate
        end
        return spriteTemplate:createSprite()
    end
    return self
end

return LPCSpriteRendererFactory