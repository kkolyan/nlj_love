return function()
    local self = {}
    local frameId = 0
    local deltas = {}
    local avFps

    function self:update(dt)

        local deltasCount = 120
        deltas[frameId % deltasCount] = dt
        frameId = frameId + 1

        local sumFps = 0
        local count = 0;
        for i = 0, deltasCount - 1 do
            if deltas[i] then
                sumFps = sumFps + (1 / deltas[i])
                count = count + 1
            end
        end
        avFps = sumFps / count
    end

    function self:draw()
        local r, g, b, a = love.graphics.getColor()
        love.graphics.setColor(1, 1, 1, 1)
        love.graphics.print(string.format("FPS: %.03f", avFps), 0, 0)
        love.graphics.setColor(r, g, b, a)
    end
    return self
end