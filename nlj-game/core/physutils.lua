local physutils = {}

---@param env ObjectEnv
---@param tile TiledMapTile
function physutils.configureStaticObstacle(env, tile, userData)

    local region = tile.region
    local body = love.physics.newBody(env:getPhysicsWorld(), tile.position.x + region.width / 2, tile.position.y + region.height / 2, "static")
    local shape = love.physics.newRectangleShape(region.width, region.height)
    local fixture = love.physics.newFixture(body, shape)
    fixture:setUserData(userData)
end

return physutils