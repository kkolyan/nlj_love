---@type ObjectDef[]
local critters = {
    {
        name = "knight",
        props = {
            thrustDamage = 40,
            health = 100,
            magicDamage = 200,
            magicDistance = 1000,
            thrustDistance = 48,
            party = "Guard"
        },
        rendererName = "LPCSprite",
        spriteDef = {
            scale = { x = 1, y = 1 },
            fps = 20,
            items = {
                skin = "ext/lpc/body/male/light.png",
                feet = "ext/lpc/feet/armor/male/metal_boots_male.png",
                legs = "ext/lpc/legs/armor/male/metal_pants_male.png",
                arms = "ext/lpc/torso/plate/arms_male.png",
                chest = "ext/lpc/torso/plate/chest_male.png",
                headgear = "ext/lpc/head/helms/male/metal_helm_male.png",
                hands = "ext/lpc/hands/gloves/male/metal_gloves_male.png",
                weapon1 = "ext/lpc/weapons/both hand/spear.png",
                weapon2 = "ext/lpc/weapons/left hand/male/shield_male_cutoutforbody.png"
            },
            clips = {
                hands_up = { sequence = "spellcast", loop = false, range = { 1, 6 }, speed = 0.5 },
                hands_down = { sequence = "spellcast", loop = false, range = { 1, 6 }, reversed = true, speed = 2, overstay = 6 },
            }
        }
    },
    {
        name = "lancelot",
        parent = "knight",
        spriteDef = {
            items = {
                hair = "ext/lpc/hair/male/long/blonde.png",
                headgear = "-",
                weapon2 = "-"
            }
        },
        props = {
            party = "PC",
            moveSpeed = 1.2
        }
    },
    {
        name = "sandro",
        parent = "knight",
        health = 500,
        spriteDef = {
            scale = { x = 1.2, y = 1.2 },
            items = {
                skin = "ext/lpc/body/male/skeleton.png",
                weapon2 = "-",
                chest = "-",
                headgear = "ext/lpc/head/hoods/male/cloth_hood_male.png"
            }
        },
        props = {
            party = "None"
        }
    }
}
return critters