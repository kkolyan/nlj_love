<?xml version="1.0" encoding="UTF-8"?>
<tileset name="critters" tilewidth="76" tileheight="76" tilecount="12" columns="4">
 <tileoffset x="-38" y="9"/>
 <image source="../../devenv/map_tiles/critters.png" width="304" height="228"/>
 <tile id="0" type="character">
  <properties>
   <property name="name" value="knight"/>
   <property name="orientation" value="UP"/>
  </properties>
 </tile>
 <tile id="1" type="character">
  <properties>
   <property name="name" value="knight"/>
   <property name="orientation" value="LEFT"/>
  </properties>
 </tile>
 <tile id="2" type="character">
  <properties>
   <property name="name" value="knight"/>
   <property name="orientation" value="DOWN"/>
  </properties>
 </tile>
 <tile id="3" type="character">
  <properties>
   <property name="name" value="knight"/>
   <property name="orientation" value="RIGHT"/>
  </properties>
 </tile>
 <tile id="4" type="character">
  <properties>
   <property name="name" value="lancelot"/>
   <property name="orientation" value="UP"/>
  </properties>
 </tile>
 <tile id="5" type="character">
  <properties>
   <property name="name" value="lancelot"/>
   <property name="orientation" value="LEFT"/>
  </properties>
 </tile>
 <tile id="6" type="character">
  <properties>
   <property name="name" value="lancelot"/>
   <property name="orientation" value="DOWN"/>
  </properties>
 </tile>
 <tile id="7" type="character">
  <properties>
   <property name="name" value="lancelot"/>
   <property name="orientation" value="RIGHT"/>
  </properties>
 </tile>
 <tile id="8" type="character">
  <properties>
   <property name="name" value="sandro"/>
   <property name="orientation" value="UP"/>
  </properties>
 </tile>
 <tile id="9" type="character">
  <properties>
   <property name="name" value="sandro"/>
   <property name="orientation" value="LEFT"/>
  </properties>
 </tile>
 <tile id="10" type="character">
  <properties>
   <property name="name" value="sandro"/>
   <property name="orientation" value="DOWN"/>
  </properties>
 </tile>
 <tile id="11" type="character">
  <properties>
   <property name="name" value="sandro"/>
   <property name="orientation" value="RIGHT"/>
  </properties>
 </tile>
</tileset>