local PcBehavior = require("game.character.behaviors.PcBehavior")
local CharacterObject = require("game.character.CharacterObject")
local utils = require("core.utils")
local critters = require("game.critters")
local physutils = require("core.physutils")
local GuardBehavior = require("game.character.behaviors.GuardBehavior")

local GameMapController = {}

function GameMapController.new()

    ---@class GameMapController : MapController
    local self = {}

    function self:init(env)
        ---@param object CharacterObject
        for _, object in ipairs(env:getObjects("critters")) do
            local party = object:getInitialProps().party
            if party == "PC" then
                object:setBehavior(PcBehavior.new())
            elseif party == "Guard" then
                object:setBehavior(GuardBehavior.new())
            end
        end
        for _, tile in ipairs(env:getTiles("impassable")) do
            physutils.configureStaticObstacle(env, tile, tile)
        end
        for _, tile in ipairs(env:getTiles("walls")) do
            physutils.configureStaticObstacle(env, tile, tile)
        end
    end

    function self:createObject(env, renderer, props)
        if props.type == "character" then
            return CharacterObject.new(env, renderer, props)
        end
    end

    function self:getObjectDefs()
        return critters
    end

    function self:beforeUpdate(dt) end

    function self:afterUpdate(dt) end

    return self
end

return GameMapController