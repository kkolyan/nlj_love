local utils = require("core.utils")

local StateManager = {}

---@type fun():StateManager
StateManager.new = function()

    --- @class StateManager
    local self = {}

    local stack = {}

    ---@param State
    function self:push(state)
        if not state then
            error("state is nil")
        end
        table.insert(stack, state)
    end

    --- @return CharacterState
    function self:peek()
        return stack[#stack]
    end

    function self:pop()
        local state = self:peek()
        table.remove(stack, #stack)
        return state
    end

    function self:doState(dt)
        self:peek():update(self, dt)
    end
    return self
end

return StateManager