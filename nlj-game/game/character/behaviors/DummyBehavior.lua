local utils = require("core.utils")

local DummyBehavior = {}

DummyBehavior.new = function()

    ---@class DummyBehavior : Behavior
    local self = {}
    function self:getIntendedMovementDirection(object)
        return nil
    end

    function self:getIntendedAction(object)
        return nil
    end
    function self:beforeUpdate(dt, object) end
    function self:afterUpdate(dt, object) end

    return self
end

return DummyBehavior