local utils = require("core.utils")
local actions = require("game.character.actions")

local PcBehavior = {}

PcBehavior.new = function()

    ---@class PcBehavior : Behavior
    local self = {}

    function self:getIntendedMovementDirection(object)
        local moveDirection = { x = 0, y = 0 }
        if love.keyboard.isDown("w") then
            moveDirection.y = moveDirection.y - 1
        end
        if love.keyboard.isDown("s") then
            moveDirection.y = moveDirection.y + 1
        end
        if love.keyboard.isDown("a") then
            moveDirection.x = moveDirection.x - 1
        end
        if love.keyboard.isDown("d") then
            moveDirection.x = moveDirection.x + 1
        end
        local moveAmp = utils.vectorLength(moveDirection)
        if moveAmp > 0 then
            return moveDirection
        end
        return nil
    end

    function self:getIntendedAction(object)

        if love.keyboard.isDown("lctrl") then
            return actions.thrust(object)
        end
        if love.keyboard.isDown("space") then
            return actions.kedavra(object)
        end
        return nil
    end

    function self:beforeUpdate(dt, object) end
    function self:afterUpdate(dt, object) end

    return self
end

return PcBehavior