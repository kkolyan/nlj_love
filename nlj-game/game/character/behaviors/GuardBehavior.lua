local utils = require("core.utils")
local actions = require("game.character.actions")
local GuardBehavior = {}

function GuardBehavior.new()
    ---@class GuardBehavior : Behavior
    local self = {}

    ---@type Vec2
    local lastSeenAt
    local seeRightNowAt

    local function isAlarmed()
        return lastSeenAt and true or false
    end

    local function distanceEnoughToAttach(object)
        return object:getInitialProps().thrustDistance * 0.8
    end

    function self:getIntendedAction(object)
        if seeRightNowAt then
            if utils.vectorDistance(seeRightNowAt, object:getPosition()) < distanceEnoughToAttach(object) then
                return actions.thrust(object)
            end
        end
    end

    local function distanceToLast(object)
        return utils.vectorDistance(lastSeenAt, object:getPosition())
    end

    local function snapTo45(angleDeg)
        return utils.round(angleDeg / 45) * 45
    end

    function self:getIntendedMovementDirection(object)
        if lastSeenAt then
            local dist = distanceToLast(object)
            if (not seeRightNowAt and dist > 10) or dist >= distanceEnoughToAttach(object) then
                local direct = utils.vectorMinus(lastSeenAt, object:getPosition())
                local angleDeg = utils.vectorAngleDegrees(direct)
                local snappedDeg = snapTo45(angleDeg)
                return utils.vectorOfAngle(snappedDeg, 1)
            end
        end
    end

    function self:beforeUpdate(dt, object)
        seeRightNowAt = nil
        ---@type Vec2[]
        local checkDirs = {}
        if isAlarmed() then
            for i = 0, 359, 10 do
                table.insert(checkDirs, utils.vectorOfAngle(i, 1))
            end
        else
            checkDirs = { object:getDirection() }
        end
        for _, checkDir in ipairs(checkDirs) do
            local hit = object:rayCast(utils.vectorMultiply(checkDir, 1000))
            if hit then
                ---@type CharacterObject
                local suspect = hit.object
                if suspect.typeof_CharacterObject then
                    if suspect:getInitialProps().party ~= object:getInitialProps().party then
                        lastSeenAt = suspect:getPosition()
                        seeRightNowAt = suspect:getPosition()
                    end
                end
            end
        end
    end

    function self:afterUpdate(dt, object) end

    return self
end

return GuardBehavior