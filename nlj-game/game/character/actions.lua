local actions = {}

local attack

---@param object CharacterObject
function actions.thrust(object)

    local props = object:getInitialProps()
    ---@type ActionIntent
    local intent = {}
    intent.stanceOn = "thrust_raise"
    intent.preEffect = "thrust_loop_before_hit"
    intent.postEffect = "thrust_loop_after_hit"
    intent.stanceOff = "thrust_put"
    intent.family = "thrust"
    function intent:applyEffect()
        attack(object, props.thrustDistance, props.thrustDamage)
    end

    return intent
end

---@param object CharacterObject
function actions.kedavra(object)
    local props = object:getInitialProps()

    ---@type ActionIntent
    local intent = {}
    intent.preEffect = "hands_up"
    intent.postEffect = "hands_down"
    intent.family = "spellcast"
    function intent:applyEffect()
        attack(object, props.magicDistance, props.magicDamage)
    end
    return intent
end

---@param object CharacterObject
function attack(object, distance, damage)
    local direction = object:getDirection()
    local hit = object:rayCast(
            {
                x = direction.x * distance,
                y = direction.y * distance
            },
            function(fixture, x, y, xn, yn, frstate)
                return 1
            end)

    if hit then
        --- @type CharacterObject
        local character = hit.object
        if character.typeof_CharacterObject then
            character:receiveDamage(damage)
            character:flyOff(direction, damage * 1000000)
        end
    end
end

return actions