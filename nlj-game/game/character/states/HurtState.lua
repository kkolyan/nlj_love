local HurtState = {}

---@type fun(actor:CharacterObject):CharacterState
HurtState.new = function(actor)

    ---@class unhurt : CharacterState
    local unhurt = {}
    function unhurt:update(context, dt)
        if actor:getSprite().isClipFinished() then
            context:pop()
            context:doState(dt)
        end
    end

    ---@class hurt : CharacterState
    local hurt = {}
    function hurt:update(context, dt)
        if actor:getSprite().isClipFinished() then
            context:pop()
            actor:getSprite():setActiveClip("unhurt")
            context:push(unhurt)
            context:doState(dt)
        end
    end

    ---@class HurtState : CharacterState
    local self = {}
    function self:update(context, dt)
        context:pop()

        actor:getSprite():setActiveClip("hurt")
        context:push(hurt)
    end
    return self
end
return HurtState