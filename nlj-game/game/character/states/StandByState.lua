local utils = require("core.utils")
local ActionState = require("game.character.states.ActionState")

local StandByState = {}

--- @type fun(actor:CharacterObject, env:ObjectEnv):CharacterState
StandByState.new = function(actor, env)

    assert(actor)

    --- @class StandByState : CharacterState
    local self = {}
    function self:update(context, dt)
        if actor:getHealth() <= 0 then
            context:pop()
            actor:getSprite():setActiveClip("knock")

            actor:setDeadSince(os.time())
        else
            local direction = actor:getBehavior():getIntendedMovementDirection(actor)
            local actionIntent = actor:getBehavior():getIntendedAction(actor)
            if actionIntent then
                context:push(ActionState.new(actor, actionIntent))
                context:doState(dt)
            elseif direction then
                actor:walk(direction)
                actor:getSprite():setOrientationByVector(direction)
                actor:getSprite():setActiveClip("walk")
            else
                actor:getSprite():setActiveClip("stand")
            end
        end
    end
    return self
end

return StandByState
