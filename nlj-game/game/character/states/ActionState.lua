local utils = require("core.utils")

local ThrustState = {}
---@type fun(actor:CharacterObject,actionIntent:ActionIntent):CharacterState
ThrustState.new = function(actor, actionIntent)
    assert(actor)

    local function setClipOrIgnore(clipName)
        if clipName then
            actor:getSprite():setActiveClip(clipName)
        end
    end

    ---@class finishThrust : CharacterState
    local postEffect = {}

    ---@class doThrust : CharacterState
    local preEffect = {}

    function postEffect:update(context, dt)
        if not actionIntent.postEffect or actor:getSprite().isClipFinished() then
            context:pop()

            local newActionIntent = actor:getBehavior():getIntendedAction(actor)
            if newActionIntent and newActionIntent.family == actionIntent.family then
                actionIntent = newActionIntent
                setClipOrIgnore(actionIntent.preEffect)
                context:push(preEffect)
                context:doState(dt)
            else
                context:pop()
                setClipOrIgnore(actionIntent.stanceOff)
                context:doState(dt)
            end
        end
    end

    function preEffect:update(context, dt)
        if not actionIntent.preEffect or actor:getSprite().isClipFinished() then
            actionIntent:applyEffect()
            context:pop()
            context:push(postEffect)
            setClipOrIgnore(actionIntent.postEffect)
            context:doState(dt)
        end
    end

    ---@class stanceOff : CharacterState
    local stanceOff = {}
    function stanceOff:update(context, dt)
        if not actionIntent.stanceOff or actor:getSprite().isClipFinished() then
            context:pop()
            context:doState(dt)
        end
    end

    ---@class stanceOn : CharacterState
    local stanceOn = {}
    function stanceOn:update(context, dt)
        if not actionIntent.stanceOn or actor:getSprite().isClipFinished() then
            setClipOrIgnore(actionIntent.preEffect)
            context:push(preEffect)
            context:doState(dt)
        end
    end

    ---@class ThrustState : CharacterState
    local self = {}
    function self:update(context, dt)
        context:pop()
        setClipOrIgnore(actionIntent.stanceOn)
        context:push(stanceOff)
        context:push(stanceOn)
    end
    return self
end

return ThrustState