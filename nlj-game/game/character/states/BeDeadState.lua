local utils = require("core.utils")

local BeDeadState = {}

---@type fun(actor:CharacterObject):CharacterState
BeDeadState.new = function(actor)

    --- @class BeDeadState : CharacterState
    local self = { }
    function self:update(context, dt) end
    return self
end

return BeDeadState
