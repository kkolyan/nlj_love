---@class ActionIntent
---@field stanceOn
---@field preEffect
---@field postEffect
---@field stanceOff
---@field family @ actions with the same family can be chained without stance off
local ActionIntent = {}

function ActionIntent:applyEffect() end
