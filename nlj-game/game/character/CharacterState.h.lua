
--- @class CharacterState
local State = {}

--- @param context StateManager
function State:update(context, dt) end

--- @param context StateManager
function State:interrupt(context) end