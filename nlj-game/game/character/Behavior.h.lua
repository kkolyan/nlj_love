
---@class Behavior
local Behavior = {}

---@param object CharacterObject
---@return Vec2
function Behavior:getIntendedMovementDirection(object) end

---@param object CharacterObject
---@return ActionIntent
function Behavior:getIntendedAction(object) end

---@param object CharacterObject
function Behavior:beforeUpdate(dt, object) end

---@param object CharacterObject
function Behavior:afterUpdate(dt, object) end
