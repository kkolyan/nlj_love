local utils = require("core.utils")
local spriteSpec = require("core.lpc.sprite.spec")
local StateManager = require("game.character.StateManager")
local StandByState = require("game.character.states.StandByState")
local BeDeadState = require("game.character.states.BeDeadState")
local HurtState = require("game.character.states.HurtState")
local DummyBehavior = require("game.character.behaviors.DummyBehavior")

local CharacterObject = {}

--- @param env ObjectEnv
--- @param sprite CompositeSprite
--- @return CharacterObject
function CharacterObject.new(env, sprite, initialProps)

    ---@class CharacterObject : MapObject
    local self = {}
    self.typeof_CharacterObject = true

    sprite:setActiveClip("stand")

    local stateManager = StateManager.new()

    local health = initialProps.health

    --- @type Behavior
    local behavior = DummyBehavior.new()

    local body = love.physics.newBody(env:getPhysicsWorld(), initialProps.x, initialProps.y, "dynamic")
    body:setLinearDamping(50)
    local k = math.sqrt(sprite:getScaleX() * sprite:getScaleY())
    body:setMass(10 * k)
    local radius = 12 * k
    local shape = love.physics.newCircleShape(radius)
    local fixture = love.physics.newFixture(body, shape)
    fixture:setRestitution(0.4)
    fixture:setUserData(self)

    stateManager:push(BeDeadState.new(self))

    stateManager:push(StandByState.new(self, env))

    local orientationByName = utils.indexListBy(spriteSpec.orientations, "name")
    local orientationName = initialProps.orientation
    local orientation = orientationByName[orientationName]
    sprite:setOrientationByVector(orientation.initialVector)
    local direction = orientation.initialVector

    local deadSince

    function self:beforeUpdate(dt)
        behavior:beforeUpdate(dt, self)
        stateManager:doState(dt)
    end

    function self:afterUpdate(dt)
        sprite:setPosition({ x = body:getX(), y = body:getY() })

        if utils.debugPhysics then
            utils.debugCircle(sprite:getX(), sprite:getY(), shape:getRadius())
        end

        if deadSince then
            sprite:setZIndex(-900000000000000000 + deadSince)
        else
            sprite:setZIndex(sprite:getY())
        end
        behavior:afterUpdate(dt, self)
    end

    function self:walk(walkDir)
        direction = utils.vectorNormalize(walkDir)
        local power = 10 * math.pow(radius, 2) * k
        body:applyForce(
                direction.x * power,
                direction.y * power
        )
    end

    function self:flyOff(flyDir, amplitude)
        direction = utils.vectorNormalize(flyDir)
        local power = 10 * math.pow(radius, 2) * k
        body:applyForce(
                direction.x * amplitude / power,
                direction.y * amplitude / power
        )
    end

    function self:receiveDamage(damage)
        health = health - damage
        if not deadSince then
            stateManager:push(HurtState.new(self))
        end
    end

    function self:setDeadSince(value)
        fixture:destroy()
        env:changeLayer(self, "corpses")
        deadSince = value
    end

    ---@return CompositeSprite
    function self:getSprite()
        return sprite
    end

    ---@param rayDir Vec2
    ---@return RayCastHit
    function self:rayCast(rayDir)
        assert(rayDir)
        local x = body:getX()
        local y = body:getY()

        ---@type RayCastHit[]
        local hits = {}

        ---@type fun(collision:Fixture, x, y, xn, yn, frstate):number
        local callback = function(fixture, x, y, xn, yn, frstate)
            table.insert(hits, { point = { x = x, y = y }, object = fixture:getUserData() })
            return 1
        end

        if utils.debugPhysics then
            utils.debugLine(x, y, x + rayDir.x, y + rayDir.y)
        end

        env:getPhysicsWorld():rayCast(
                x, y,
                x + rayDir.x, y + rayDir.y,
                callback)

        local here = { x = x, y = y }

        table.sort(hits, function(a, b)
            return utils.vectorDistance(a.point, here) < utils.vectorDistance(b.point, here)
        end)

        return hits[1]
    end

    function self:getHealth()
        return health
    end

    function self:getBehavior()
        return behavior
    end

    ---@param value Behavior
    function self:setBehavior(value)
        behavior = value
    end

    function self:getInitialProps()
        return utils.deepClone(initialProps)
    end

    ---@return Vec2
    function self:getDirection()
        return utils.deepClone(direction)
    end

    function self:getPosition()
        return { x = sprite:getX(), y = sprite:getY() }
    end

    function self:__tostring()
        return string.format("Character {%s, %s,%s}", initialProps.name, sprite:getX(), sprite:getY())
    end

    return self
end

return CharacterObject